# Dynamic DNS with AWS 

This software allows you to update the records in your Route53 hosted zones in order to create your own Dynamic DNS Service.

## Installation

0. Create your hosted zone and record in Route53
1. Create a new Lambda function in AWS and select API Gateway as a trigger. A new API Gateway with proxy integration will be created. Allow public access.
2. Modify both index.js files and replace the constants so that it reflects your needs.
#### ./lambda/index.js
```
let config = {
    "host.dyndns.your-domain.tld": {
        "HostedZoneId": 'YOUR-HOSTED-ZONE-ID (ROUTE53)',
        "ttl": 60, // increase if you want
        "type": "A", // Type of the record. eg. A, CNAME, ...
        "auth": "YOUR-SECURE-TOKEN"
    }
};

/*
 * Configure the AWS AccessID and AccessKey. This keypair needs write access for route 53
 */
AWS.config.update({
    accessKeyId:        'ACCESS-KEY',
    secretAccessKey:    'SECRET-ACCESS-KEY'
});
// AWS Region (unnecessary for route53, but in case you need more AWS Services it is nice to set it early on)
AWS.config.update({region: 'eu-west-1'});
```
You will find the hosted zone id in the summary of Route53.
As 'auth' set something secure as everyone who has access to it will be able to change the record.
If you want then you can add multiple dynamic dns entries.

#### ./client/index.js
```
const domain = "host.dyndns.your-domain.tld";
const auth = "YOUR-SECURE-TOKEN";
const api_endpoint = 'API-GATEWAY-ENDPOINT';
const interval = 5 * 60 * 1000;
```
* 'domain' and 'auth' need to correspond with the settings in the lambda function.
* API-GATEWAY-ENDPOINT is the HTTPS endpoint with which the lambda function can be called
* The public IP address will be checked for change every interval ms.

3. Upload Lambda Function. As a test event you can set the following JSON:

```
{
   "queryStringParameters": {
     "ip": "9.9.9.9",
     "auth": "YOUR-AUTH-TOKEN",
     "domain": "host.dyndns.your-domain.tld"
   },
   "sourceIP": "8.8.8.8"
 }
```

## Dependencies
Nodejs
 
## Usage
Run ./client/index.js or make https requests to your endpoint with the following GET parameters:
* ip - new IP address
* auth - auth token for domain
* domain - domain for which the record should be updated

## Problems
If you've run into any problems, please contact me via mail.

## Licence
MIT, see `LICENCE`

If you need another licence, contact me.
