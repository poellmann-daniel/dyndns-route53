'use strict';

/*
 * Load modules
 */
let AWS = require('aws-sdk');
let dns = require('dns');

/*
 * Configure the allowed records and their auth token which is needed in order to make changes.
 */
let config = {
    "host.dyndns.your-domain.tld": {
        "HostedZoneId": 'HOSTED-ZONE-ID',
        "ttl": 60,
        "type": "A",
        "auth": "AUTH-TOKEN"
    }
};

/*
 * Configure the AWS AccessID and AccessKey. This keypair needs write access for route 53
 */
AWS.config.update({
    accessKeyId:        'ACCESS_KEY',
    secretAccessKey:    'SECRET-ACCESS-KEY'
});
// AWS Region (unnecessary for route53, but in case you need more AWS Services it is nice to set it early on)
AWS.config.update({region: 'eu-west-1'});

exports.handler = function(event, context, callback) {

    if (event.queryStringParameters !== null && event.queryStringParameters !== undefined) {

        if (event.queryStringParameters.ip      != undefined    &&
            event.queryStringParameters.auth    != undefined    &&
            event.queryStringParameters.domain  != undefined    ) {

            let ip      = event.queryStringParameters.ip;
            let auth    = event.queryStringParameters.auth;
            let domain  = event.queryStringParameters.domain;

            // Check if domain is allowed and authentication
            if (config[domain] != undefined) {

                if (auth == config[domain]['auth']) {

                    // Before any changes are sent to Route53 we first check the latest IP address and
                    // compare it to the new one. If they are already the same, we can ignore the update
                    dns.lookup(domain, function (err, address) {

                        if (address != ip) {

                            let route53 = new AWS.Route53();
                            let params = {
                                "HostedZoneId": config[domain]["HostedZoneId"],
                                "ChangeBatch": {
                                    "Changes": [
                                        {
                                            "Action": "UPSERT",
                                            "ResourceRecordSet": {
                                                "Name": domain,
                                                "Type": config[domain]['type'],
                                                "TTL": config[domain]["ttl"],
                                                "ResourceRecords": [
                                                    {
                                                        "Value": ip
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            };

                            route53.changeResourceRecordSets(params, function(err,data) {

                                if (!err) {

                                    finished({
                                        message: "ok",
                                    }, callback);

                                } else {

                                    finished({
                                        message: "route53error",
                                    }, callback);

                                }

                            });


                        } else {

                            if (!err) {
                                finished({
                                    message: "ok",
                                }, callback);

                            } else {

                                finished({
                                    message: "dnserror",
                                }, callback);

                            }
                        }

                    });

                } else {

                    finished({
                        message: "autherror",
                    }, callback);

                }

            }

    } else {

        finished({
            message: "badrequest",
        }, callback);

        }

}

    function finished(responseBody, callback) {
        let responseCode = 200;
        let response = {
            statusCode: responseCode,
            headers: {  "Access-Control-Allow-Origin" : "*"},
        body: JSON.stringify(responseBody)
        };
        callback(null, response);
    }


};