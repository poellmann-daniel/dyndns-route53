const domain = "host.dyndns.your-domain.tld";
const auth = "AUTH-TOKEN";
const api_endpoint = 'https://XXX.execute-api.eu-west-1.amazonaws.com/prod/XXX';
const interval = 5 * 60 * 1000;

const publicIp = require('public-ip');
const request = require('request');
let last_ip = "";

function update() {

        publicIp.v4().then(ip => { // IPv6? Then Change it :)
            console.log("My IP is: " + ip);

            if (last_ip != ip) {
                console.log("IP changed since last check. New IP is: " + ip);

                request(api_endpoint + "?ip="+ip+"&auth="+auth+"&domain="+domain, function (error, response, body) {

                    if (!error && JSON.parse(body)['message'] == "ok") {
                        console.log("Update successful");
                        last_ip = ip;
                    } else {
                        console.log("An error occurred:");

                        console.log('error:', error); // Print the error if one occurred
                        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                        console.log('body:', body); // Print the HTML for the Google homepage.
                    }

                });
            } else {
                console.log("IP did not change since last check.");
            }

        });
}

setInterval(update, interval);
update();